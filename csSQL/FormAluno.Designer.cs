﻿namespace csSQL
{
    partial class FormAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.txtNAluno = new System.Windows.Forms.TextBox();
            this.txtProc = new System.Windows.Forms.TextBox();
            this.lblnAluno = new System.Windows.Forms.Label();
            this.lblnProc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(13, 184);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(108, 184);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(251, 33);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(100, 20);
            this.txtNome.TabIndex = 11;
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(184, 33);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(38, 13);
            this.lblNome.TabIndex = 10;
            this.lblNome.Text = "Nome:";
            // 
            // txtNAluno
            // 
            this.txtNAluno.Location = new System.Drawing.Point(65, 64);
            this.txtNAluno.Name = "txtNAluno";
            this.txtNAluno.Size = new System.Drawing.Size(100, 20);
            this.txtNAluno.TabIndex = 9;
            // 
            // txtProc
            // 
            this.txtProc.Enabled = false;
            this.txtProc.Location = new System.Drawing.Point(65, 33);
            this.txtProc.Name = "txtProc";
            this.txtProc.Size = new System.Drawing.Size(100, 20);
            this.txtProc.TabIndex = 8;
            // 
            // lblnAluno
            // 
            this.lblnAluno.AutoSize = true;
            this.lblnAluno.Location = new System.Drawing.Point(15, 67);
            this.lblnAluno.Name = "lblnAluno";
            this.lblnAluno.Size = new System.Drawing.Size(44, 13);
            this.lblnAluno.TabIndex = 7;
            this.lblnAluno.Text = "nºAluno";
            // 
            // lblnProc
            // 
            this.lblnProc.AutoSize = true;
            this.lblnProc.Location = new System.Drawing.Point(15, 33);
            this.lblnProc.Name = "lblnProc";
            this.lblnProc.Size = new System.Drawing.Size(39, 13);
            this.lblnProc.TabIndex = 6;
            this.lblnProc.Text = "nºProc";
            // 
            // FormAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 262);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.txtNAluno);
            this.Controls.Add(this.txtProc);
            this.Controls.Add(this.lblnAluno);
            this.Controls.Add(this.lblnProc);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnOk);
            this.Name = "FormAluno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormAluno";
            this.Load += new System.EventHandler(this.FormAluno_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.TextBox txtNAluno;
        private System.Windows.Forms.TextBox txtProc;
        private System.Windows.Forms.Label lblnAluno;
        private System.Windows.Forms.Label lblnProc;
    }
}