﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SQLite;


namespace csSQL
{
    public partial class FormCicloLetico : Form
    {
        private SqlConnection sqlConn = null;
        private MySqlConnection mySqlConn = null;
        private SQLiteConnection sqLiteConn = null;
        private string sgbd = "";
        private string dmlSelect = "";
        private string cod = "";


        /*CONSTRUTOR
         Alteração do construtor por defeito, para receber 3 strings:
         - bd: Indentifica o sgbd a comunicar
         - dmlSelect: contem a SQL DML a executar : (Insert, Update, Delete)
         - cod: do registo a alterar ou eliminar
         Origem:
         - Menu vem um pedido dmlSelect = "Insert", para um determinado "sgbd", com cod = "".
         - FormLista vem um sgbd = "Update" ou "Delete" para um "sgbd", para um codigo selecionado na dataGriedView
         */

        public FormCicloLetico(string sgbd, string dmlSelect, string cod)
        {
            InitializeComponent();
            this.sgbd = sgbd;
            this.dmlSelect = dmlSelect;
            this.cod = cod;
        }

        //LOAD : 1º método a ser executado, depois do construtor
        private void FormCicloLetico_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + this.sgbd + " - " + this.dmlSelect ;
            MessageBox.Show("Vem de sgbd " + this.sgbd + " DML" + this.dmlSelect);



            this.Text = this.Text + " " + sgbd + " " + dmlSelect + " " + cod;
            MessageBox.Show("Clicou no menu " + sgbd + " com a opcao " + dmlSelect);
            
            
            switch (dmlSelect)
            {
                
                case "Insert":
                    btnOk.Text = dmlSelect;
                    txtCiclo.Text = getLastTablePk();
                    txtDescricao.Focus();
                    break;
                case "Update":
                    btnOk.Text = dmlSelect;
                    getFieldsData();
                    txtCiclo.Text = cod;
                    txtDescricao.Focus();
                    break;
                case "Delete":
                    btnOk.Text = dmlSelect;
                    getFieldsData();
                    txtCiclo.Text = cod;
                    txtCiclo.Enabled = false;
                    txtDescricao.Enabled = false;
                    break;
                default: if (UtilsSQL.getTeste()) MessageBox.Show("DML SELCT Indeterminado: " + dmlSelect, "ERRO FormLoad - Load - switch");
                    break;
            }


            //Se insert, query à BD para obter o ultimo pk

            //Se Update, usar código passdo no construtor para recolher os dados da BD

            //Se Delete, usar o código passado no construtor para recolher os dados da BD
            //Definir os campos Disable
        }

        private string getLastTablePk()
        {
            int pkCode = -1;                                                        //Recebe o codigo da tabela a usar nas SQL
            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd);      //Pede a UtilsSQL uma ligação ao SGBD
            try
            {
                if (UtilsSQL.getTeste()) MessageBox.Show("DML = " + dmlSelect + "\n Vou fazer a query ", "Testes");
                SqlCommand sqlCommand = new SqlCommand("Select MAX(nCiclo) as nCiclo from CicloLetivo", sqlConn); //ComandoSQL
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        string temp = sqlDataReader["nCiclo"].ToString();
                        if (UtilsSQL.getTeste()) MessageBox.Show("Max pkCode na Tabela =" + temp, "Testes, FormCicloLetivo - getLastPk()");
                        if (temp == "")
                        {
                            MessageBox.Show("Tabela Vazia. Ok para inserir o 1º registo", "Info");
                            pkCode = 1;
                        }
                        else
                        {
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou converter " + temp + " para INT", "Testes; FormCicloLetivo - getLastTablePk()");
                            pkCode = int.Parse(temp) + 1;
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Tabela Vazia 2. Ok para inserir o 1º registo", "Info");
                    pkCode = 1;
                }
                return pkCode.ToString();           //Registo na TextBox da codigo para  a pk
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source.ToString() + "\n" + ex.TargetSite.ToString() + "\n" + ex.Message, "Erro: Form CicloLetivo - getLastTabelPk()");
                return pkCode.ToString();
            }
            finally
            {

                sqlConn.Close();
            }
        }

        public void getFieldsData()
        {
            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd);
            try
            {
                if (UtilsSQL.getTeste()) MessageBox.Show("DML = " + dmlSelect + "\n Vou fazer a query", "TESTES: getFieldsData");

                //Comando Sql DML
                SqlCommand sqlCommand = new SqlCommand("Select Descr, nome from CicloLetivo where nCiclo= @codigo", sqlConn);
                sqlCommand.Parameters.AddWithValue("@codigo", cod);
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        txtDescricao.Text = sqlDataReader["Descr"].ToString();
                    }
                }
                else MessageBox.Show("Não foram encontrados dados para o registo de código: " + cod, "INFO");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro BD: \n" + ex.Message, "FormCicloLetivo - getFieldsData()");
            }
            finally
            {
                sqlConn.Close();
            }
        }


	    


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.Show();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd);
            SqlTransaction sqlTran = null;          //transação para controlo sql

            if (txtCiclo.Text == "" || txtDescricao.Text == "" )
            {
                MessageBox.Show("Não são permitidos campos vazios", "Atenção");
            }
            else
            {
                try
                {
                    switch (dmlSelect)
                    {
                        case "Insert":
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML SELECT", "TESTES");

                            SqlCommand sqlInsert = new SqlCommand("Insert into CicloLetivo (nCiclo,Descr) VALUES(@nCiclo,@Descr)", sqlConn);
                            sqlInsert.Parameters.AddWithValue("@nCiclo", int.Parse(getLastTablePk()));
                            sqlInsert.Parameters.AddWithValue("@Descr", txtDescricao.Text);

                            sqlConn.Open();                         //Aberta a ligação a BD
                            sqlTran = sqlConn.BeginTransaction();   //Transação para controlo da operação SQL
                            sqlInsert.Transaction = sqlTran;        //Ligação dos comandos a transação
                            sqlInsert.ExecuteNonQuery();             //eXECUTA O SQL DML
                            sqlTran.Commit();

                            break;

                        case "Update":
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML UPDATE", "TESTES");

                            SqlCommand sqlUpdate = new SqlCommand("Update CicloLetivo SET Descr = @Descr, WHERE nCiclo = @nCiclo ", sqlConn);
                            sqlUpdate.Parameters.AddWithValue("@Descr", txtDescricao.Text);
                            sqlUpdate.Parameters.AddWithValue("@nCiclo", int.Parse(cod));



                            sqlConn.Open();                             //Aberta a ligação a BD
                            sqlUpdate.ExecuteNonQuery();                 //eXECUTA O SQL DML

                            break;

                        case "Delete":
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML DELETE", "TESTES");

                            SqlCommand sqlDelete = new SqlCommand("Delete from Turma WHERE nCiclo = @nCiclo", sqlConn);
                            sqlDelete.Parameters.AddWithValue("@nCiclo", int.Parse(cod));



                            sqlConn.Open();                         //Aberta a ligação a BD
                            sqlDelete.ExecuteNonQuery();            //eXECUTA O SQL DML

                            break;



                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Erro Bd:\n" + ex.Message, "ERRO: FormCicloLetivo - BOTAO OK - SWITCH");
                    try
                    {
                        sqlTran.Rollback();

                    }
                    catch (Exception exRollback)
                    {
                        MessageBox.Show("Erro Bd:\n" + exRollback.Message, "ERRO: FormCicloLetivo - BOTAO OK - SWITCH");
                    }
                }
                finally
                {
                    sqlConn.Close();
                    MessageBox.Show("Procedimento Concluido", "INFO");
                    Form1 form1 = new Form1();
                    this.Hide();
                    form1.Show();

                }
            }
        }

       
    }
}
