﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace csSQL
{
    public partial class FormTurma : Form
    {
        private string sgbd = "";
        private string dmlSelect = "";
        private string cod = "";


        /*CONSTRUTOR
         Alteração do construtor por defeito, para receber 3 strings:
         - bd: Indentifica o sgbd a comunicar
         - dmlSelect: contem a SQL DML a executar : (Insert, Update, Delete)
         - cod: do registo a alterar ou eliminar
         Origem:
         - Menu vem um pedido dmlSelect = "Insert", para um determinado "sgbd", com cod = "".
         - FormLista vem um sgbd = "Update" ou "Delete" para um "sgbd", para um codigo selecionado na dataGriedView
         */

        public FormTurma(string sgbd , string dmlSelect , string cod )
        {
            InitializeComponent();
            this.sgbd = sgbd;
            this.dmlSelect = dmlSelect;
            this.cod = cod;
        }

        //LOAD : 1º método a ser executado, depois do construtor
        private void FormTurma_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + this.sgbd + " - " + this.dmlSelect ;
            MessageBox.Show("Vem de sgbd " + this.sgbd + " DML" + this.dmlSelect);

            this.Text = this.Text + " " + sgbd + " " + dmlSelect + " " + cod;
            MessageBox.Show("Clicou no menu " + sgbd + " com a opcao " + dmlSelect);


            switch (dmlSelect)
            {
                case "Insert":
                    btnOk.Text = dmlSelect;
                    txtNTurma.Text = getLastTablePk();
                    cbCicloLetivo.Focus();
                    break;
                case "Update":
                    btnOk.Text = dmlSelect;
                    getFieldsData();
                    txtNTurma.Text = cod;
                    cbCicloLetivo.Focus();
                    break;
                case "Delete":
                    btnOk.Text = dmlSelect;
                    getFieldsData();
                    txtNTurma.Text = cod;
                    txtNTurma.Enabled = false;
                    cbCicloLetivo.Enabled = false;
                    txtDescricao.Enabled = false;
                    txtAnoLetivo.Enabled = false;
                    break;
                default: if (UtilsSQL.getTeste()) MessageBox.Show("DML SELCT Indeterminado: " + dmlSelect, "ERRO FormLoad - Load - switch");
                    break;
            }

            //Se insert, query à BD para obter o ultimo pk

            //Se Update, usar código passdo no construtor para recolher os dados da BD

            //Se Delete, usar o código passado no construtor para recolher os dados da BD
            //Definir os campos Disable

            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd);
            


            SqlCommand sqlCommand = new SqlCommand("Select * from CicloLetivo", sqlConn);
          //  sqlCommand.Parameters.AddWithValue("@codigo", cod);
            sqlCommand.CommandType = CommandType.Text;

            sqlConn.Open();
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                   cbCicloLetivo.Items.Add(sqlDataReader["nCiclo"].ToString());
                }           
            }
            else MessageBox.Show("Não foram encontrados dados para o registo de código: " + cod, "INFO");
            sqlConn.Close();



        }

        private string getLastTablePk()
        {
            int pkCode = -1;
            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd);

            try
            {
                if (UtilsSQL.getTeste()) MessageBox.Show("DML = " + dmlSelect + "\n Vou fazer a query");

                SqlCommand sqlCommand = new SqlCommand("Select MAX(nTurma) as nTurma from Turma", sqlConn);
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        string temp = sqlDataReader["nTurma"].ToString();
                        if (UtilsSQL.getTeste()) MessageBox.Show("Max pkCode na Tabela = " + temp, "TESTES Form Turma");

                        if (temp == "")
                        {
                            MessageBox.Show("Tabela Vazia OK para inserir o 1º registo", "INFO");
                            pkCode = 1;
                        }
                        else
                        {
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou converter " + temp + "para int", "Testes FormTurma");
                            pkCode = int.Parse(temp) + 1;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Tabela vazia 2. Ok para inserir o 1º registo", "INFO");
                    pkCode = 1;
                }
                return pkCode.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source.ToString() + "\n" + ex.TargetSite.ToString() + "\n" + ex.Message, "ERRO Form Turma - getLastTablePk");
                return pkCode.ToString();
            }
            finally
            {
                sqlConn.Close();
            }
        }

        public void getFieldsData()
        {
            SqlConnection sqlCoon = (SqlConnection)UtilsSQL.selectSgbd(sgbd);

            try
            {
                if (UtilsSQL.getTeste()) MessageBox.Show("DML = " + dmlSelect + "\n Vou fazer a query");

                //comando Sql DML
                SqlCommand sqlCommand = new SqlCommand("Select CicloLetivonCiclo, Descri, AnoLetivo from Turma where nTurma = @cod", sqlCoon);
                sqlCommand.Parameters.AddWithValue("@cod", cod);
                sqlCommand.CommandType = CommandType.Text;

                sqlCoon.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        cbCicloLetivo.Text = sqlDataReader["CicloLetivonCiclo"].ToString();
                        txtDescricao.Text = sqlDataReader["Descri"].ToString();
                        txtAnoLetivo.Text = sqlDataReader["AnoLetivo"].ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Nao foram encontrados dados para o registo de codigo " + cod, "INFO");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro BD: \n" + ex.Message, "FormTurma - getFieldsData()");
            }
            finally
            {
                sqlCoon.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.Show();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd);
            SqlTransaction sqlTran = null;

            if (txtDescricao.Text == "" || cbCicloLetivo.Text == "" || txtNTurma.Text == "" || txtAnoLetivo.Text == "")
            {
                MessageBox.Show("Nao sao permitidos camos vazios", "Atencao");
            }
            else
            {
                try
                {
                    switch (dmlSelect)
                    {
                        case "Insert":
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer o DML: " + dmlSelect);

                            SqlCommand sqlInsert = new SqlCommand("Insert into Turma (nTurma,CicloLetivonCiclo,Descri,AnoLetivo) VALUES(@nTurma,@CicloLetivonCiclo, @Descri, @AnoLetivo)", sqlConn);
                            sqlInsert.Parameters.AddWithValue("@nTurma", int.Parse(getLastTablePk()));
                            sqlInsert.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(cbCicloLetivo.Text));
                            sqlInsert.Parameters.AddWithValue("@Descri", txtDescricao.Text);
                            sqlInsert.Parameters.AddWithValue("@AnoLetivo", int.Parse(txtAnoLetivo.Text));

                            sqlConn.Open();
                            sqlTran = sqlConn.BeginTransaction();
                            sqlInsert.Transaction = sqlTran;
                            sqlInsert.ExecuteNonQuery();
                            sqlTran.Commit();

                            break;
                        case "Update":
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer o DML: " + dmlSelect);

                            SqlCommand sqlUpdate = new SqlCommand("Update Turma SET CicloLetivonCiclo = @CicloLetivonCiclo, Descri = @Descri, AnoLetivo = @AnoLetivo WHERE nTurma = @nTurma", sqlConn);
                            sqlUpdate.Parameters.AddWithValue("@CicloLetivonCiclo", int.Parse(cbCicloLetivo.Text));
                            sqlUpdate.Parameters.AddWithValue("@Descri", txtDescricao.Text);
                            sqlUpdate.Parameters.AddWithValue("@nTurma", int.Parse(cod));
                            sqlUpdate.Parameters.AddWithValue("@AnoLetivo", int.Parse(txtAnoLetivo.Text));

                            sqlConn.Open();
                            sqlUpdate.ExecuteNonQuery();

                            break;
                        case "Delete":
                            if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer o DML: " + dmlSelect);

                            SqlCommand sqlDelete = new SqlCommand("Delete from Turma where nTurma = @nTurma", sqlConn);
                            sqlDelete.Parameters.AddWithValue("@nTurma", cod);

                            sqlConn.Open();
                            sqlDelete.ExecuteNonQuery();

                            break;
                        default:
                            MessageBox.Show("DML indeterminado: " + dmlSelect, "ERRO");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro Bd: \n" + ex.Message, "Erro FormAluno Botao ok");
                    try
                    {
                        sqlTran.Rollback();
                    }
                    catch (Exception exRollBack)
                    {
                        MessageBox.Show("Erro Bd: \n" + exRollBack.Message, "Erro FormAluno Botao ok");
                    }
                }
                finally
                {
                    sqlConn.Close();
                    MessageBox.Show("Procedimento Concluido", "INFO");
                    Form1 form1 = new Form1();
                    this.Hide();
                    form1.Show();
                }
            }
        }

        
    }
}
