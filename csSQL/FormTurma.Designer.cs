﻿namespace csSQL
{
    partial class FormTurma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.txtNTurma = new System.Windows.Forms.TextBox();
            this.lblnTurma = new System.Windows.Forms.Label();
            this.lblCicloLetivo = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtAnoLetivo = new System.Windows.Forms.TextBox();
            this.lblAnoLetivo = new System.Windows.Forms.Label();
            this.cbCicloLetivo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(263, 9);
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(100, 20);
            this.txtDescricao.TabIndex = 19;
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Location = new System.Drawing.Point(202, 12);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(55, 13);
            this.lblDescricao.TabIndex = 18;
            this.lblDescricao.Text = "Descrição";
            // 
            // txtNTurma
            // 
            this.txtNTurma.Location = new System.Drawing.Point(73, 43);
            this.txtNTurma.Name = "txtNTurma";
            this.txtNTurma.Size = new System.Drawing.Size(100, 20);
            this.txtNTurma.TabIndex = 17;
            // 
            // lblnTurma
            // 
            this.lblnTurma.AutoSize = true;
            this.lblnTurma.Location = new System.Drawing.Point(10, 46);
            this.lblnTurma.Name = "lblnTurma";
            this.lblnTurma.Size = new System.Drawing.Size(50, 13);
            this.lblnTurma.TabIndex = 15;
            this.lblnTurma.Text = "nº Turma";
            // 
            // lblCicloLetivo
            // 
            this.lblCicloLetivo.AutoSize = true;
            this.lblCicloLetivo.Location = new System.Drawing.Point(10, 12);
            this.lblCicloLetivo.Name = "lblCicloLetivo";
            this.lblCicloLetivo.Size = new System.Drawing.Size(62, 13);
            this.lblCicloLetivo.TabIndex = 14;
            this.lblCicloLetivo.Text = "Ciclo Letivo";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(103, 163);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(8, 163);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 12;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtAnoLetivo
            // 
            this.txtAnoLetivo.Location = new System.Drawing.Point(263, 47);
            this.txtAnoLetivo.Name = "txtAnoLetivo";
            this.txtAnoLetivo.Size = new System.Drawing.Size(100, 20);
            this.txtAnoLetivo.TabIndex = 21;
            // 
            // lblAnoLetivo
            // 
            this.lblAnoLetivo.AutoSize = true;
            this.lblAnoLetivo.Location = new System.Drawing.Point(202, 50);
            this.lblAnoLetivo.Name = "lblAnoLetivo";
            this.lblAnoLetivo.Size = new System.Drawing.Size(58, 13);
            this.lblAnoLetivo.TabIndex = 20;
            this.lblAnoLetivo.Text = "Ano Letivo";
            // 
            // cbCicloLetivo
            // 
            this.cbCicloLetivo.FormattingEnabled = true;
            this.cbCicloLetivo.Location = new System.Drawing.Point(73, 9);
            this.cbCicloLetivo.Name = "cbCicloLetivo";
            this.cbCicloLetivo.Size = new System.Drawing.Size(121, 21);
            this.cbCicloLetivo.TabIndex = 22;
            // 
            // FormTurma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 262);
            this.Controls.Add(this.cbCicloLetivo);
            this.Controls.Add(this.txtAnoLetivo);
            this.Controls.Add(this.lblAnoLetivo);
            this.Controls.Add(this.txtDescricao);
            this.Controls.Add(this.lblDescricao);
            this.Controls.Add(this.txtNTurma);
            this.Controls.Add(this.lblnTurma);
            this.Controls.Add(this.lblCicloLetivo);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnOk);
            this.Name = "FormTurma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormTurma";
            this.Load += new System.EventHandler(this.FormTurma_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.TextBox txtNTurma;
        private System.Windows.Forms.Label lblnTurma;
        private System.Windows.Forms.Label lblCicloLetivo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtAnoLetivo;
        private System.Windows.Forms.Label lblAnoLetivo;
        private System.Windows.Forms.ComboBox cbCicloLetivo;
    }
}