﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csSQL
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (UtilsSQL.getTeste()) 
                radioButtonOn.Checked = true;
            else 
                radioButtonOff.Checked = true;


            if (UtilsSQL.getBdOnline())
            {
                radioButtonOnline.Checked = false;
                menuDataSet.Enabled = false;
                menuBDlocal.Enabled = false;
                menuMySql.Enabled = true;
                menuSqlLite.Enabled = false;
                menuSqlServer.Enabled = false;
                menuOracle.Enabled = false;
            }
            else
            {
                radioButtonLocal.Checked = true;
                menuDataSet.Enabled = true;
                menuBDlocal.Enabled = true;
                menuSqlLite.Enabled = true;
                menuSqlServer.Enabled = true;
                menuOracle.Enabled = true;
            }
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void menuDataSetAluno_Click(object sender, EventArgs e)
        {
            FormDataSetAluno formDataSetAluno = new FormDataSetAluno();
            this.Hide();
            formDataSetAluno.Show();
        }

        private void menuDataSetTurma_Click(object sender, EventArgs e)
        {
            FormDataSetTurma formDataSetTurma = new FormDataSetTurma();
            this.Hide();
            formDataSetTurma.Show();
        }

        //ALUNO-----------------------------------------------------------------------------------------

        private void menuBDlocalAlunoInserir_Click(object sender, EventArgs e)
        {
            FormAluno formAluno = new FormAluno("BDlocal","Insert","");
            this.Hide();
            formAluno.Show();
        }

        private void menuSQLServerAlunoInserir_Click(object sender, EventArgs e)
        {
            FormAluno formAluno = new FormAluno("SQLServer", "Insert", "");
            this.Hide();
            formAluno.Show();
        }

        private void menuMySqlAlunoInserir_Click(object sender, EventArgs e)
        {
            FormAluno formAluno = new FormAluno("MySql", "Insert", "");
            this.Hide();
            formAluno.Show();
        }

        private void menuSQLLiteAlunoInserir_Click(object sender, EventArgs e)
        {
            FormAluno formAluno = new FormAluno("SQLLite", "Insert", "");
            this.Hide();
            formAluno.Show();
        }

        private void menuOracleAlunoInserir_Click(object sender, EventArgs e)
        {
            FormAluno formAluno = new FormAluno("Oracle", "Insert", "");
            this.Hide();
            formAluno.Show();
        }

        private void menuBDlocalAlunoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("BDlocal", "ALuno");
            this.Hide();
            formLista.Show();
        }

        private void menuSQLServerAlunoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("SQLServer", "ALuno");
            this.Hide();
            formLista.Show();
        }

        private void menuMySqlAlunoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("MySql", "ALuno");
            this.Hide();
            formLista.Show();
        }

        private void menuSQLLiteAlunoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("SQLLite", "ALuno");
            this.Hide();
            formLista.Show();
        }

        private void menuOracleAlunoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("Oracle", "ALuno");
            this.Hide();
            formLista.Show();
        }

        //TURMA-----------------------------------------------------------------------------------------

        private void menuBDlocalTurmaInserir_Click(object sender, EventArgs e)
        {
            FormTurma formTurma = new FormTurma("BDlocal", "Insert", "");
            this.Hide();
            formTurma.Show();
        }

        private void menuSQLServerTurmaInserir_Click(object sender, EventArgs e)
        {
            FormTurma formTurma = new FormTurma("SQLServer", "Insert", "");
            this.Hide();
            formTurma.Show();
        }

        private void menuMySqlTurmaInserir_Click(object sender, EventArgs e)
        {
            FormTurma formTurma = new FormTurma("MySql", "Insert", "");
            this.Hide();
            formTurma.Show();
        }

        private void menuSQLLiteTurmaInserir_Click(object sender, EventArgs e)
        {
            FormTurma formTurma = new FormTurma("SQLLite", "Insert", "");
            this.Hide();
            formTurma.Show();
        }

        private void menuOracleTurmaInserir_Click(object sender, EventArgs e)
        {
            FormTurma formTurma = new FormTurma("Oracle", "Insert", "");
            this.Hide();
            formTurma.Show();
        }

        private void menuSQLServerTurmaConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("SQLServer", "Turma");
            this.Hide();
            formLista.Show();
        }

        private void menuMySqlConsultarTurma_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("MySql", "Turma");
            this.Hide();
            formLista.Show();
        }

        private void menuSQLLiteTurmaConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("SQLLite", "Turma");
            this.Hide();
            formLista.Show();
        }

        private void menuOracleTurmaConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("Oracle", "Turma");
            this.Hide();
            formLista.Show();
        } 
        
        private void menuBDlocalTurmaConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("BDlocal", "Turma");
            this.Hide();
            formLista.Show();
        }

        //CICLOLETIVO----------------------------------------------------------------------------------- 

        private void menuBDlocalCicloLeticoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("BDlocal", "CicloLetivo");
            this.Hide();
            formLista.Show();
        }

        private void menuSQLServerCIcloLetivoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("SQLServer", "CicloLetivo");
            this.Hide();
            formLista.Show();
        }

        private void menuMySqlCicloLetivoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("MySql", "CicloLetivo");
            this.Hide();
            formLista.Show();
        }

        private void menuSQLLiteCicloLetivoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("SQLLite", "CicloLetivo");
            this.Hide();
            formLista.Show();
        }

        private void menuOracleCicloLetivoConsultar_Click(object sender, EventArgs e)
        {
            FormLista formLista = new FormLista("Oracle", "CicloLetivo");
            this.Hide();
            formLista.Show();
        }

        private void menuBDlocalCicloLetivoInserir_Click(object sender, EventArgs e)
        {
            FormCicloLetico formCicloLetivo = new FormCicloLetico("BDlocal", "Insert", "");
            this.Hide();
            formCicloLetivo.Show();
        }

        private void menuSQLServerCicloLetivoInserir_Click(object sender, EventArgs e)
        {
            FormCicloLetico formCicloLetivo = new FormCicloLetico("SQLServer", "Insert", "");
            this.Hide();
            formCicloLetivo.Show();
        }

        private void menuMySqlCicloLetivoInserir_Click(object sender, EventArgs e)
        {
            FormCicloLetico formCicloLetivo = new FormCicloLetico("MySql", "Insert", "");
            this.Hide();
            formCicloLetivo.Show();
        }

        private void menuSQLLiteCicloLetivoInserir_Click(object sender, EventArgs e)
        {
            FormCicloLetico formCicloLetivo = new FormCicloLetico("SQLLite", "Insert", "");
            this.Hide();
            formCicloLetivo.Show();
        }

        private void menuOracleCicloLetivoInserir_Click(object sender, EventArgs e)
        {
            FormCicloLetico formCicloLetivo = new FormCicloLetico("Oracle", "Insert", "");
            this.Hide();
            formCicloLetivo.Show();
        }

        private void radioButtonOn_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonOn.Checked) UtilsSQL.setTeste(true);
            else UtilsSQL.setTeste(false);
        }

        private void radioButtonLocal_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLocal.Checked)
            {
                UtilsSQL.setBdOnline(false);

                menuDataSet.Enabled = true;
                menuBDlocal.Enabled = true;
                menuMySql.Enabled = true;
                menuSqlLite.Enabled = true;
                menuSqlServer.Enabled = true;
                menuOracle.Enabled = true;
            }
            else
            {
                UtilsSQL.setBdOnline(true);

                menuDataSet.Enabled = false;
                menuBDlocal.Enabled = false;
                menuMySql.Enabled = true;
                menuSqlLite.Enabled = false;
                menuSqlServer.Enabled = false;
                menuOracle.Enabled = false;
            }
        }

        private void radioButtonOnline_CheckedChanged(object sender, EventArgs e)
        {

        }

       

       
    }
}
