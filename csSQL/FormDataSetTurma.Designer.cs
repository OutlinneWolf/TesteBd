﻿namespace csSQL
{
    partial class FormDataSetTurma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDataSetTurma));
            System.Windows.Forms.Label nTurmaLabel;
            System.Windows.Forms.Label cicloLetivonCicloLabel;
            System.Windows.Forms.Label descriLabel;
            System.Windows.Forms.Label anoLetivoLabel;
            this.btnVoltar = new System.Windows.Forms.Button();
            this.bDlocalDataSet = new csSQL.BDlocalDataSet();
            this.turmaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.turmaTableAdapter = new csSQL.BDlocalDataSetTableAdapters.TurmaTableAdapter();
            this.tableAdapterManager = new csSQL.BDlocalDataSetTableAdapters.TableAdapterManager();
            this.turmaBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.turmaBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.turmaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nTurmaTextBox = new System.Windows.Forms.TextBox();
            this.cicloLetivonCicloTextBox = new System.Windows.Forms.TextBox();
            this.descriTextBox = new System.Windows.Forms.TextBox();
            this.anoLetivoTextBox = new System.Windows.Forms.TextBox();
            nTurmaLabel = new System.Windows.Forms.Label();
            cicloLetivonCicloLabel = new System.Windows.Forms.Label();
            descriLabel = new System.Windows.Forms.Label();
            anoLetivoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingNavigator)).BeginInit();
            this.turmaBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.turmaDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnVoltar
            // 
            this.btnVoltar.Location = new System.Drawing.Point(537, 28);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(75, 23);
            this.btnVoltar.TabIndex = 3;
            this.btnVoltar.Text = "voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // bDlocalDataSet
            // 
            this.bDlocalDataSet.DataSetName = "BDlocalDataSet";
            this.bDlocalDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // turmaBindingSource
            // 
            this.turmaBindingSource.DataMember = "Turma";
            this.turmaBindingSource.DataSource = this.bDlocalDataSet;
            // 
            // turmaTableAdapter
            // 
            this.turmaTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ALuno_TurmaTableAdapter = null;
            this.tableAdapterManager.ALunoTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CicloLetivoTableAdapter = null;
            this.tableAdapterManager.TurmaTableAdapter = this.turmaTableAdapter;
            this.tableAdapterManager.UpdateOrder = csSQL.BDlocalDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // turmaBindingNavigator
            // 
            this.turmaBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.turmaBindingNavigator.BindingSource = this.turmaBindingSource;
            this.turmaBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.turmaBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.turmaBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.turmaBindingNavigatorSaveItem});
            this.turmaBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.turmaBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.turmaBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.turmaBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.turmaBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.turmaBindingNavigator.Name = "turmaBindingNavigator";
            this.turmaBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.turmaBindingNavigator.Size = new System.Drawing.Size(632, 25);
            this.turmaBindingNavigator.TabIndex = 4;
            this.turmaBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 15);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // turmaBindingNavigatorSaveItem
            // 
            this.turmaBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.turmaBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("turmaBindingNavigatorSaveItem.Image")));
            this.turmaBindingNavigatorSaveItem.Name = "turmaBindingNavigatorSaveItem";
            this.turmaBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.turmaBindingNavigatorSaveItem.Text = "Save Data";
            this.turmaBindingNavigatorSaveItem.Click += new System.EventHandler(this.turmaBindingNavigatorSaveItem_Click);
            // 
            // turmaDataGridView
            // 
            this.turmaDataGridView.AutoGenerateColumns = false;
            this.turmaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.turmaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.turmaDataGridView.DataSource = this.turmaBindingSource;
            this.turmaDataGridView.Location = new System.Drawing.Point(221, 66);
            this.turmaDataGridView.Name = "turmaDataGridView";
            this.turmaDataGridView.Size = new System.Drawing.Size(399, 164);
            this.turmaDataGridView.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "nTurma";
            this.dataGridViewTextBoxColumn1.HeaderText = "nTurma";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CicloLetivonCiclo";
            this.dataGridViewTextBoxColumn2.HeaderText = "CicloLetivonCiclo";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Descri";
            this.dataGridViewTextBoxColumn3.HeaderText = "Descri";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "AnoLetivo";
            this.dataGridViewTextBoxColumn4.HeaderText = "AnoLetivo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // nTurmaLabel
            // 
            nTurmaLabel.AutoSize = true;
            nTurmaLabel.Location = new System.Drawing.Point(12, 55);
            nTurmaLabel.Name = "nTurmaLabel";
            nTurmaLabel.Size = new System.Drawing.Size(49, 13);
            nTurmaLabel.TabIndex = 4;
            nTurmaLabel.Text = "n Turma:";
            // 
            // nTurmaTextBox
            // 
            this.nTurmaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "nTurma", true));
            this.nTurmaTextBox.Location = new System.Drawing.Point(67, 52);
            this.nTurmaTextBox.Name = "nTurmaTextBox";
            this.nTurmaTextBox.Size = new System.Drawing.Size(100, 20);
            this.nTurmaTextBox.TabIndex = 5;
            // 
            // cicloLetivonCicloLabel
            // 
            cicloLetivonCicloLabel.AutoSize = true;
            cicloLetivonCicloLabel.Location = new System.Drawing.Point(12, 85);
            cicloLetivonCicloLabel.Name = "cicloLetivonCicloLabel";
            cicloLetivonCicloLabel.Size = new System.Drawing.Size(97, 13);
            cicloLetivonCicloLabel.TabIndex = 5;
            cicloLetivonCicloLabel.Text = "Ciclo Letivon Ciclo:";
            // 
            // cicloLetivonCicloTextBox
            // 
            this.cicloLetivonCicloTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "CicloLetivonCiclo", true));
            this.cicloLetivonCicloTextBox.Location = new System.Drawing.Point(115, 82);
            this.cicloLetivonCicloTextBox.Name = "cicloLetivonCicloTextBox";
            this.cicloLetivonCicloTextBox.Size = new System.Drawing.Size(100, 20);
            this.cicloLetivonCicloTextBox.TabIndex = 6;
            // 
            // descriLabel
            // 
            descriLabel.AutoSize = true;
            descriLabel.Location = new System.Drawing.Point(12, 114);
            descriLabel.Name = "descriLabel";
            descriLabel.Size = new System.Drawing.Size(40, 13);
            descriLabel.TabIndex = 7;
            descriLabel.Text = "Descri:";
            // 
            // descriTextBox
            // 
            this.descriTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "Descri", true));
            this.descriTextBox.Location = new System.Drawing.Point(58, 111);
            this.descriTextBox.Name = "descriTextBox";
            this.descriTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriTextBox.TabIndex = 8;
            // 
            // anoLetivoLabel
            // 
            anoLetivoLabel.AutoSize = true;
            anoLetivoLabel.Location = new System.Drawing.Point(12, 143);
            anoLetivoLabel.Name = "anoLetivoLabel";
            anoLetivoLabel.Size = new System.Drawing.Size(61, 13);
            anoLetivoLabel.TabIndex = 9;
            anoLetivoLabel.Text = "Ano Letivo:";
            // 
            // anoLetivoTextBox
            // 
            this.anoLetivoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "AnoLetivo", true));
            this.anoLetivoTextBox.Location = new System.Drawing.Point(79, 140);
            this.anoLetivoTextBox.Name = "anoLetivoTextBox";
            this.anoLetivoTextBox.Size = new System.Drawing.Size(100, 20);
            this.anoLetivoTextBox.TabIndex = 10;
            // 
            // FormDataSetTurma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 262);
            this.Controls.Add(anoLetivoLabel);
            this.Controls.Add(this.anoLetivoTextBox);
            this.Controls.Add(descriLabel);
            this.Controls.Add(this.descriTextBox);
            this.Controls.Add(cicloLetivonCicloLabel);
            this.Controls.Add(this.cicloLetivonCicloTextBox);
            this.Controls.Add(nTurmaLabel);
            this.Controls.Add(this.nTurmaTextBox);
            this.Controls.Add(this.turmaDataGridView);
            this.Controls.Add(this.turmaBindingNavigator);
            this.Controls.Add(this.btnVoltar);
            this.Name = "FormDataSetTurma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDataSetTurma";
            this.Load += new System.EventHandler(this.FormDataSetTurma_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingNavigator)).EndInit();
            this.turmaBindingNavigator.ResumeLayout(false);
            this.turmaBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.turmaDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVoltar;
        private BDlocalDataSet bDlocalDataSet;
        private System.Windows.Forms.BindingSource turmaBindingSource;
        private BDlocalDataSetTableAdapters.TurmaTableAdapter turmaTableAdapter;
        private BDlocalDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator turmaBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton turmaBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView turmaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.TextBox nTurmaTextBox;
        private System.Windows.Forms.TextBox cicloLetivonCicloTextBox;
        private System.Windows.Forms.TextBox descriTextBox;
        private System.Windows.Forms.TextBox anoLetivoTextBox;
    }
}