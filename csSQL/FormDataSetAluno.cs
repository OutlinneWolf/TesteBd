﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csSQL
{
    public partial class FormDataSetAluno : Form
    {
        public FormDataSetAluno()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.Show();
        }

        private void FormDataSetAluno_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bDlocalDataSet.ALuno' table. You can move, or remove it, as needed.
            this.aLunoTableAdapter.Fill(this.bDlocalDataSet.ALuno);

        }

        private void aLunoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.aLunoBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bDlocalDataSet);

        }

        private void nomeLabel_Click(object sender, EventArgs e)
        {

        }

        private void nomeTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void aLunoDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
