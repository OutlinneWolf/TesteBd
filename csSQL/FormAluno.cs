﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SQLite;

namespace csSQL
{
    /*Esta Form é chamada po:
     * - qualquer das opcoes "inserir Alunos", dos vários menus;
     * - FormLista para alterar ou eliminar registos  existentes
     * Tem um comportamento múltiplo, atraves do construtor da Form e o metodo Load()
     * - o construtor é alterado e recebe 3 parametros
     * - o metodo load() 1º dos metodos a ser executado apos o construtor , prepara a form;
     * - o metodo click do botão ok, executa o sql e dml, de acordo com dmlSelect recibo
     * */

    public partial class FormAluno : Form
    {
        private SqlConnection sqlConn = null;
        //private MySqlConnection mySqlConn = null;
        private SQLiteConnection sqLiteConn = null;

        private SqlTransaction sqlTran = null;
        private MySqlTransaction mySqlTran = null;
        private SQLiteTransaction sqLiteTran = null;

        private string sgbd = "";
        private string dmlSelect = "";
        private string cod = "";


        /*CONSTRUTOR
         Alteração do construtor por defeito, para receber 3 strings:
         - bd: Indentifica o sgbd a comunicar
         - dmlSelect: contem a SQL DML a executar : (Insert, Update, Delete)
         - cod: do registo a alterar ou eliminar
         Origem:
         - Menu vem um pedido dmlSelect = "Insert", para um determinado "sgbd", com cod = "".
         - FormLista vem um sgbd = "Update" ou "Delete" para um "sgbd", para um codigo selecionado na dataGriedView
         */

        public FormAluno(string sgbd, string dmlSelect, string cod)
        {
            InitializeComponent();
            this.sgbd = sgbd;
            this.dmlSelect = dmlSelect;
            this.cod = cod;
        }

        //LOAD : 1º método a ser executado, depois do construtor
        /* - Altera  e preenche a barra de titulo
         * - Altera o nome do botao ok, conforme o atributo dmlSelect, definido no construtor
         * - Prepara a form de acordo com o dmlSelect
         *      - Insert: Query a tabela para obter o novo pk; Campos Enable
         *      - Update: Query a tabela obtem os dados do registo referete ao codigo. Cmpos Enable
         *      - Delete: Query a tabela obtem os dados do registo referente ao codigo. Campos Disable*/
        private void FormAluno_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + this.sgbd + " - " + this.dmlSelect;


            switch (dmlSelect)                              //Determina qual é a DML a executar no botão
            {
                case "Insert":
                    btnOk.Text = dmlSelect;
                    txtProc.Text = getLastTablePk();          //Obtêm o ultimo valor da tabela para o novo registo
                    txtNome.Focus();                        //Focus nesta caixa de texto
                    break;

                case "Update":
                    btnOk.Text = dmlSelect;
                    getFieldsData();
                    txtProc.Text = cod;
                    txtNome.Focus();
                    break;

                case "Delete":
                    btnOk.Text = dmlSelect;
                    getFieldsData();
                    txtNome.Enabled = false;
                    txtNAluno.Enabled = false;
                    txtProc.Enabled = false;
                    txtProc.Text = cod;
                    break;

                default:
                    if (UtilsSQL.getTeste()) MessageBox.Show("DMLSELECT Indeterminado: " + dmlSelect, "ERRO: FormAluno - Load - switch");
                    break;

            }

            //Se insert, query à BD para obter o ultimo pk

            //Se Update, usar código passdo no construtor para recolher os dados da BD

            //Se Delete, usar o código passado no construtor para recolher os dados da BD
            //Definir os campos Disable

        }


        private string getLastTablePk()
        {
            MySqlConnection mySqlConn = null;
            int pkCode = -1;                                                        //Recebe o codigo da tabela a usar nas SQL
                  //Pede a UtilsSQL uma ligação ao SGBD
            try
            {
                switch (sgbd)
                {
                    case "BDlocal":
                    case "SQLServer":
                        sqlConn = UtilsSQL.getSqlConnSQLServer();
                        if (UtilsSQL.getTeste()) MessageBox.Show("DML = " + dmlSelect + "\n Vou fazer a query ", "Testes");
                        SqlCommand sqlCommand = new SqlCommand("Select MAX(nProc) as nProc from Aluno", sqlConn); //ComandoSQL
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                        if (sqlDataReader.HasRows)
                        {
                            while (sqlDataReader.Read())
                            {
                                string temp = sqlDataReader["nProc"].ToString();
                                if (UtilsSQL.getTeste()) MessageBox.Show("Max pkCode na Tabela =" + temp, "Testes, FormAluno - getLastPk()");
                                if (temp == "")
                                {
                                    MessageBox.Show("Tabela Vazia. Ok para inserir o 1º registo", "Info");
                                    pkCode = 1;
                                }
                                else
                                {
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou converter " + temp + " para INT", "Testes; FormAluno - getLastTablePk()");
                                    pkCode = int.Parse(temp) + 1;
                                }

                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. Ok para inserir o 1º registo", "Info");
                            pkCode = 1;
                        }
                        break;

                    case "MySql":
                        mySqlConn = UtilsSQL.getSqlConnMySql();
                        if (UtilsSQL.getTeste()) MessageBox.Show("DML = " + dmlSelect + "\n Vou fazer a query ", "Testes");
                        MySqlCommand mySqlCommand = new MySqlCommand("Select MAX(nProc) as nProc from ALuno", mySqlConn); //ComandoSQL
                        mySqlCommand.CommandType = CommandType.Text;

                        mySqlConn.Open();
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                        if (mySqlDataReader.HasRows)
                        {
                            while (mySqlDataReader.Read())
                            {
                                string temp = mySqlDataReader["nProc"].ToString();
                                if (UtilsSQL.getTeste()) MessageBox.Show("Max pkCode na Tabela =" + temp, "Testes, FormAluno - getLastPk()");
                                if (temp == "")
                                {
                                    MessageBox.Show("Tabela Vazia. Ok para inserir o 1º registo", "Info");
                                    pkCode = 1;
                                }
                                else
                                {
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou converter " + temp + " para INT", "Testes; FormAluno - getLastTablePk()");
                                    pkCode = int.Parse(temp) + 1;
                                }

                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. Ok para inserir o 1º registo", "Info");
                            pkCode = 1;
                        }
                        break;
                    case "SQLLite":

                        sqLiteConn = UtilsSQL.getSqlConnSQLLite();
                        SQLiteCommand sqLiteCommand = new SQLiteCommand("Select MAX(nProc) as nProc from Aluno", sqLiteConn); //ComandoSQL
                        sqLiteCommand.CommandType = CommandType.Text;

                        sqLiteConn.Open();
                        SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader();

                        if (sqLiteDataReader.HasRows)
                        {
                            while (sqLiteDataReader.Read())
                            {
                                string temp = sqLiteDataReader["nProc"].ToString();
                                if (UtilsSQL.getTeste()) MessageBox.Show("Max pkCode na Tabela =" + temp, "Testes, FormAluno - getLastPk()");
                                if (temp == "")
                                {
                                    MessageBox.Show("Tabela Vazia. Ok para inserir o 1º registo", "Info");
                                    pkCode = 1;
                                }
                                else
                                {
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou converter " + temp + " para INT", "Testes; FormAluno - getLastTablePk()");
                                    pkCode = int.Parse(temp) + 1;
                                }

                            }
                        }
                        else
                        {
                            MessageBox.Show("Tabela Vazia 2. Ok para inserir o 1º registo", "Info");
                            pkCode = 1;
                        }
                        break;
                    case "Oracle":

                        break;
                    default:
                        MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source.ToString() + "\n" + ex.TargetSite.ToString() + "\n" + ex.Message, "Erro: Form Aluno - getLastTabelPk()");
            }
            finally
            {
                switch (sgbd)
                {
                    case "BDlocal":
                    case "SQLServer": 
                        sqlConn.Close();
                        break;

                    case "MySql": 
                        mySqlConn.Close();
                        break;

                    case "SQLLite": 
                        sqLiteConn.Clone();
                        break;

                    case "Oracle":
                        break;

                    default:
                        MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                        break;
                }
            }
            return pkCode.ToString();
        }






        /// <summary>
        /// 
        /// </summary>
        private void getFieldsData()
        {
            MySqlConnection mySqlConn = null;
            try
            {

                switch (sgbd)
                {
                    case "BDlocal":
                    case "SQLServer":

                        sqlConn = (SqlConnection)UtilsSQL.getSqlConnSQLServer();
                        //Comando Sql DML
                        SqlCommand sqlCommand = new SqlCommand("Select NAluno, nome from Aluno where nProc= @codigo", sqlConn);
                        sqlCommand.Parameters.AddWithValue("@codigo", cod);
                        sqlCommand.CommandType = CommandType.Text;

                        sqlConn.Open();
                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                        if (sqlDataReader.HasRows)
                        {
                            while (sqlDataReader.Read())
                            {
                                txtNome.Text = sqlDataReader["Nome"].ToString();
                                txtNAluno.Text = sqlDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show("Não foram encontrados dados para o registo de código: " + cod, "INFO");
                        break;

                    case "MySql":
                        mySqlConn = UtilsSQL.getSqlConnMySql();
                        //Comando Sql DML
                        MySqlCommand mySqlCommand = new MySqlCommand("Select NAluno, nome from ALuno where nProc= @codigo", mySqlConn);
                        mySqlCommand.Parameters.AddWithValue("@codigo", cod);
                        mySqlCommand.CommandType = CommandType.Text;

                        mySqlConn.Open();
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                        if (mySqlDataReader.HasRows)
                        {
                            while (mySqlDataReader.Read())
                            {
                                txtNome.Text = mySqlDataReader["Nome"].ToString();
                                txtNAluno.Text = mySqlDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show("Não foram encontrados dados para o registo de código: " + cod, "INFO");
                        break;

                    case "SQLLite":
                        sqLiteConn = UtilsSQL.getSqlConnSQLLite();
                        //Comando Sql DML
                        SQLiteCommand sqLiteCommand = new SQLiteCommand("Select NAluno, nome from Aluno where nProc= @codigo", sqLiteConn);
                        sqLiteCommand.Parameters.AddWithValue("@codigo", cod);
                        sqLiteCommand.CommandType = CommandType.Text;

                        sqLiteConn.Open();
                        SQLiteDataReader sqLiteDataReader = sqLiteCommand.ExecuteReader();
                        if (sqLiteDataReader.HasRows)
                        {
                            while (sqLiteDataReader.Read())
                            {
                                txtNome.Text = sqLiteDataReader["Nome"].ToString();
                                txtNAluno.Text = sqLiteDataReader["NAluno"].ToString();
                            }
                        }
                        else MessageBox.Show("Não foram encontrados dados para o registo de código: " + cod, "INFO");
                        break;


                    case "Oracle":
                        break;


                    default:
                        MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro BD: \n" + ex.Message, "FormAluno - getFieldsData()");
            }
            finally
            {
                switch (sgbd)
                {
                    case "BDlocal":
                    case "SQLServer": sqlConn.Close();

                        break;
                    case "MySql": mySqlConn.Close();

                        break;
                    case "SQLLite": sqLiteConn.Clone();

                        break;
                    case "Oracle":

                        break;
                    default:
                        MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                        break;
                }
            }
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.Show();
        }

        /*Botão OK executa a DML em conformidade com dmlSelect
         * Obtem uma ligação ao SGBD a partir de UtilsSQL e prepara uma transação
         * Faz a validação de dados para não resultar em exceções na BD
         * - Insert: (Usa uma transação, necessaria quando existe mais do que uma ação na BD)
         *      - aBRE A LIGAÇÃO SQL em Utils, innicia transação SQL e associa os comandos a executar
         *      - Executa os comando SQL e termina Transação.
         *      - NOTA: catch inclui RolllBack Transation caso exista exceção.
         *      */

        private void btnOk_Click(object sender, EventArgs e)
        {

            MySqlConnection mySqlConn = UtilsSQL.getSqlConnMySql(); 

            if (txtProc.Text == "" || txtNAluno.Text == "" || txtNome.Text == "")
            {
                MessageBox.Show("Não são permitidos campos vazios", "Atenção");
            }
            else
            {
                try
                {
                    switch (sgbd)
                    {
                        case "BDlocal":
                        case "SQLServer":
                            sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd); 
                            switch (dmlSelect)
                            {
                                case "Insert":
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML SELECT", "TESTES");

                                    SqlCommand sqlInsert = new SqlCommand("Insert into ALuno (nProc,NAluno,Nome) VALUES(@nProc,@NAluno,@Nome)", sqlConn);
                                    sqlInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    sqlInsert.Parameters.AddWithValue("@NAluno", int.Parse(txtNAluno.Text));
                                    sqlInsert.Parameters.AddWithValue("@Nome", txtNome.Text);

                                    sqlConn.Open();                         //Aberta a ligação a BD
                                    sqlTran = sqlConn.BeginTransaction();   //Transação para controlo da operação SQL
                                    sqlInsert.Transaction = sqlTran;        //Ligação dos comandos a transação
                                    sqlInsert.ExecuteNonQuery();             //eXECUTA O SQL DML
                                    sqlTran.Commit();

                                    break;

                                case "Update":
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML UPDATE", "TESTES");

                                    SqlCommand sqlUpdate = new SqlCommand("Update ALuno SET NAluno = @NAluno, nome = @Nome WHERE nProc = @nProc ", sqlConn);
                                    sqlUpdate.Parameters.AddWithValue("@NAluno", int.Parse(txtNAluno.Text));
                                    sqlUpdate.Parameters.AddWithValue("@Nome", txtNome.Text);
                                    sqlUpdate.Parameters.AddWithValue("@nProc", int.Parse(cod));



                                    sqlConn.Open();                             //Aberta a ligação a BD
                                    sqlUpdate.ExecuteNonQuery();                 //eXECUTA O SQL DML

                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML DELETE", "TESTES");

                                    SqlCommand sqlDelete = new SqlCommand("Delete from ALuno WHERE nProc = @nProc", sqlConn);
                                    sqlDelete.Parameters.AddWithValue("@nProc", int.Parse(cod));



                                    sqlConn.Open();                         //Aberta a ligação a BD
                                    sqlDelete.ExecuteNonQuery();            //eXECUTA O SQL DML

                                    break;
                            }
                            break;

                        case "MySql":
                           
                            
                            switch (dmlSelect)
                            {
                                case "Insert": 
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML SELECT", "TESTES");

                                    MySqlCommand mySqlInsert = new MySqlCommand("Insert into ALuno (nProc,NAluno,Nome) VALUES(@nProc,@NAluno,@Nome)", mySqlConn);
                                    mySqlInsert.Parameters.AddWithValue("@nProc", int.Parse(getLastTablePk()));
                                    mySqlInsert.Parameters.AddWithValue("@NAluno", int.Parse(txtNAluno.Text));
                                    mySqlInsert.Parameters.AddWithValue("@Nome", txtNome.Text);

                                    MessageBox.Show("Vou abrir a bd:"+ mySqlConn.ConnectionString.ToString());
                                    mySqlConn.Open();                         //Aberta a ligação a BD
                                    mySqlTran = mySqlConn.BeginTransaction();   //Transação para controlo da operação SQL
                                    mySqlInsert.Transaction = mySqlTran;        //Ligação dos comandos a transação
                                    mySqlInsert.ExecuteNonQuery();             //eXECUTA O SQL DML
                                    mySqlTran.Commit();

                                    break;

                                case "Update":


                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML UPDATE", "TESTES");

                                    MySqlCommand mySqlUpdate = new MySqlCommand("Update ALuno SET NAluno = @NAluno, nome = @Nome WHERE nProc = @nProc ", mySqlConn);
                                    mySqlUpdate.Parameters.AddWithValue("@NAluno", int.Parse(txtNAluno.Text));
                                    mySqlUpdate.Parameters.AddWithValue("@Nome", txtNome.Text);
                                    mySqlUpdate.Parameters.AddWithValue("@nProc", int.Parse(cod));



                                    mySqlConn.Open();                             //Aberta a ligação a BD
                                    mySqlUpdate.ExecuteNonQuery();                 //eXECUTA O SQL DML

                                    break;

                                case "Delete":
                                    if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer DML DELETE", "TESTES");

                                    MySqlCommand mySqlDelete = new MySqlCommand("Delete from ALuno WHERE nProc = @nProc", mySqlConn);
                                    mySqlDelete.Parameters.AddWithValue("@nProc", int.Parse(cod));



                                    mySqlConn.Open();                         //Aberta a ligação a BD
                                    mySqlDelete.ExecuteNonQuery();            //eXECUTA O SQL DML

                                    break;
                            }
                            break;
                        case "SQLLite":
                            
                            break;
                        case "Oracle":

                            break;
                        default:
                            MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                            break;

                    }

                    
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Erro Bd:\n" + ex.Message, "ERRO: FormAluno - BOTAO OK - SWITCH");
                    try
                    {
                        switch (sgbd)
                        {
                            case "BDlocal":
                            case "SQLServer": sqlTran.Rollback();
                                break;

                            case "MySql": mySqlTran.Rollback();
                                break;

                            case "SQLLite": sqLiteTran.Rollback();
                                break;

                            case "Oracle":
                                break;

                            default:
                                MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                                break;

                        }
                        
                    }
                    catch (Exception exRollback)
                    {
                        MessageBox.Show("Erro Bd:\n" + exRollback.Message, "ERRO: FormAluno - BOTAO OK - SWITCH");
                    }
                }
                finally
                {
                    switch (sgbd)
                    {
                        case "BDlocal":
                        case "SQLServer": sqlConn.Close();
                            break;

                        case "MySql": mySqlConn.Close();
                            break;

                        case "SQLLite": sqLiteConn.Clone();
                            break;

                        case "Oracle":
                            break;

                        default:
                            MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                            break;

                    }
                    MessageBox.Show("Procedimento Concluido", "INFO");
                    Form1 form1 = new Form1();
                    this.Hide();
                    form1.Show();

                }

            }


        }
    }
}
