﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csSQL
{
    public partial class FormDataSetTurma : Form
    {
        public FormDataSetTurma()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.Show();

        }

        private void FormDataSetTurma_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bDlocalDataSet.Turma' table. You can move, or remove it, as needed.
            this.turmaTableAdapter.Fill(this.bDlocalDataSet.Turma);

        }

        private void menuDataSetTurma_Click(object sender, EventArgs e)
        {

        }

        private void turmaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.turmaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bDlocalDataSet);

        }
    }
}
