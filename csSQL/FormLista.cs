﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SQLite;


namespace csSQL
{
    public partial class FormLista : Form
    {
        private SqlConnection sqlConn = null;
        private MySqlConnection mySqlConn = null;
        private SQLiteConnection sqLiteConn = null;

        private string sgbd = "";
        private string tabela = "";

        /*CONSTRUTOR
         Alteração do construtor por defeito, para receber 2 strings
         - bd: vai permitir selecionar a base de dados correspondentes
         - tabela: vai permitir selecionar a tabela a usar na construção da lista na DataGriewView
         */

        public FormLista(string sgbd, string tabela)
        {
            InitializeComponent();
            this.sgbd = sgbd;
            this.tabela = tabela;
        }

        //LOAD : 1º método a ser executado, depois do construtor
        /*Descrição: Constroi a DataGriedView das consultas na FormList
         * Usa os atrbutos da classe: sgbd e tabela para fazer uma query para o dataGriedView
         * */
        private void FormLista_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + this.sgbd + " - " + this.tabela ;
            MessageBox.Show("Vem de sgbd" + this.sgbd + " da tabela " + this.tabela);

            
            try
            {

                switch (sgbd)
                {
                    case "BDlocal":
                    case "SQLServer":
                        sqlConn = (SqlConnection)UtilsSQL.selectSgbd(sgbd);

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou abrir a BD", "Testes: FormLista_Load()");
                        sqlConn.Open();

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer query", "Testes: FormLista_Load()");
                        SqlDataAdapter dataAdpter = new SqlDataAdapter("Select * from " + tabela, sqlConn);

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou preencher a dataGridView", "Testes: FormLista_Load()");
                        DataTable dataTable = new DataTable();
                        dataAdpter.Fill(dataTable);
                        dataGridView.DataSource = dataTable;

                        if (UtilsSQL.getTeste()) MessageBox.Show("Preenchi a dataGridView", "Testes: FormLista_Load()");

                        break;

                    case "MySql":
                        mySqlConn = UtilsSQL.getSqlConnMySql();

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou abrir a BD", "Testes: FormLista_Load()");
                        mySqlConn.Open();

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer query", "Testes: FormLista_Load()");
                        MySqlDataAdapter mySqldataAdpter = new MySqlDataAdapter("Select * from " + tabela, mySqlConn);

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou preencher a dataGridView", "Testes: FormLista_Load()");
                        DataTable mySqlDataTable = new DataTable();
                        mySqldataAdpter.Fill(mySqlDataTable);
                        dataGridView.DataSource = mySqlDataTable;

                        if (UtilsSQL.getTeste()) MessageBox.Show("Preenchi a dataGridView", "Testes: FormLista_Load()");
                        break;

                    case "SQLLite":
                        sqLiteConn = UtilsSQL.getSqlConnSQLLite();

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou abrir a BD", "Testes: FormLista_Load()");
                        sqLiteConn.Open();

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou fazer query", "Testes: FormLista_Load()");
                        SQLiteDataAdapter aqLiteDataAdpter = new SQLiteDataAdapter("Select * from " + tabela, sqLiteConn);

                        if (UtilsSQL.getTeste()) MessageBox.Show("Vou preencher a dataGridView", "Testes: FormLista_Load()");
                        DataTable sqLiteDataTable = new DataTable();
                        aqLiteDataAdpter.Fill(sqLiteDataTable);
                        dataGridView.DataSource = sqLiteDataTable;
                        break;
                    case "Oracle":

                        break;
                    default:
                        MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao abrir a Base Dados \n" + ex.Message, "Erro: FormLista_Load()");
            }
            finally
            {

                switch (sgbd)
                {
                    case "BDlocal":
                    case "SQLServer": sqlConn.Close();
                        
                        break;
                    case "MySql": mySqlConn.Close();
                        
                        break;
                    case "SQLLite": sqLiteConn.Clone();
                        
                        break;
                    case "Oracle":
                        
                        break;
                    default:
                        MessageBox.Show("String Errada: " + sgbd, "ERRO: FormListLoad - switchFinaly");
                        break;
                
                }
                
            }

            //Se insert, query à BD para obter o ultimo pk

            //Se Update, usar código passdo no construtor para recolher os dados da BD

            //Se Delete, usar o código passado no construtor para recolher os dados da BD
            //Definir os campos Disable
        }

      

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            this.Hide();
            form1.Show();
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;                              //RECEBE O INDICE DA DATAGRIDVIEW CLICADA
                DataGridViewRow row = dataGridView.Rows[rowIndex];     //EXTRAI TODO O REGISTO DA GRIDVIEW
                txtProc.Text = row.Cells[0].Value.ToString();           //EXTRAÇÃO DO 1º E 2º CAMPO DE REGISTO
                txtCampoDois.Text = row.Cells[1].Value.ToString();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Erro ArgumentOutOfRangeException\n" + ex.Message, "FormLista-DataGriedView");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro a tratar\n" + ex.Message, "FormLista-DataGriedView");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (txtProc.Text != "") //verifica se ha dados na txtProc
            {
                switch (tabela)     //chama a form da entidade definida em tabela, da base de dados definida po sgbd
                { 
                    case "ALuno":
                        FormAluno formAluno = new FormAluno(sgbd, "Update", txtProc.Text);
                        this.Hide();
                        formAluno.Show();
                        break;

                    case "Turma":
                        FormTurma formTurma = new FormTurma(sgbd, "Update", txtProc.Text);
                        this.Hide();
                        formTurma.Show();
                        break;

                    case "CicloLetivo":
                        FormCicloLetico formCicloLetivo = new FormCicloLetico(sgbd, "Update", txtProc.Text);
                        this.Hide();
                        formCicloLetivo.Show();
                        break;

                    default:
                        MessageBox.Show("String na tabela Errada " + tabela, "ERRO: FormList-btnAlterar-switch");
                        break;

                }
            }
            else
            {
                MessageBox.Show("SELECIONE UM REGISTO: " + tabela, "ATENÇÃO");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (txtProc.Text != "") //verifica se ha dados na txtProc
            {
                switch (tabela)     //chama a form da entidade definida em tabela, da base de dados definida po sgbd
                {
                    case "ALuno":
                        FormAluno formAluno = new FormAluno(sgbd, "Delete", txtProc.Text);
                        this.Hide();
                        formAluno.Show();
                        break;

                    case "Turma":
                        FormTurma formTurma = new FormTurma(sgbd, "Delete", txtProc.Text);
                        this.Hide();
                        formTurma.Show();
                        break;

                    case "CicloLetivo":
                        FormCicloLetico formCicloLetivo = new FormCicloLetico(sgbd, "Delete", txtProc.Text);
                        this.Hide();
                        formCicloLetivo.Show();
                        break;

                    default:
                        MessageBox.Show("String na tabela Errada " + tabela, "ERRO: FormList-btnEliminar-switch");
                        break;

                }
            }
            else
            {
                MessageBox.Show("SELECIONE UM REGISTO: " + tabela, "ATENÇÃO");
            }
        }
    }

        

        

        
}


