﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SQLite;




namespace csSQL
{

    /*Classe static para nao gerar objectos e reter dados comuns 
     * Todos os seus membros devem ser static , caso contrario nao seram acessiveis
     * Adicionar livraria Windows.Forms
     * Adicionar livraria System.Data.SqlClient; para Objetos SQL

    //TEM QUE SER STATIC PARA NÃO CRIAR OBJETOS E SER ACESSIVEL A TODAS CLASSES E METODOS

    //ATRIBUTOS
    /*2 POR CADA BASE DE DADOS A USAR: USAMOS 5
     * - connection string: tem a localização a parametrod da BD
     * - Connection : converte a string  localização para algo que o sgbd possa ler
     * */
    static class UtilsSQL
    {
        //ATRIBUTOS SMP STATIC E PRIVADOS

        static bool teste = true; // ativa desativa as mensagens de testes
        static bool bdOnline = false;
        /*aTRIBUTOS de ligação a bd: por bd a contatar
         * - connectionString para o endreco e parametros  da base de dados
         * - SqlConnection para converter a string num formato interpetado pelo SGBD
         * 
         * NOTA1: para obter as connectString: propriedades da base de dados
         * NOTA2: Se localDB e usar GIT o endereço tem que ser adaptado
         * 
         * 
         * 
         * NOTA3: Inserir o caracter @ antes da string - invalida queixas do compilador quando encontrar o caracter\ na string*/

        static private string sqlConnStringBDLocal = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\BDlocal.mdf;Integrated Security=True";
        static private SqlConnection sqlConnBDLocal = new SqlConnection(sqlConnStringBDLocal);

        //SQL SERVER
        static private string sqlConnStringSQLServer = @"Data Source=HP-OE4-546349;Initial Catalog=Escola;Persist Security Info=True;User ID=tgpsi;Password=esferreira123";
        static private SqlConnection sqlConnSQLServer = new SqlConnection(sqlConnStringSQLServer);

        //MySql
        static private string sqlConnStringMySqlOnline = "SERVER=sql4.freemysqlhosting.net;port=3306;DATABASE=sql498439;username=sql498439;password=T2TVJtTufH";
        static private string sqlConnStringMySqlLocal = @"server=localhost;user id=root;database=Escola";
        static private MySqlConnection sqlConnMySql = null;

        //SQL Lite
        static private string sqlConnStringSQLLite = "data source="+System.IO.Path.GetFullPath(@"..\..\")+"Escola";
        static private SQLiteConnection sqlConnSQLLite = new SQLiteConnection(sqlConnStringSQLLite);

        //Oracle
        static private string sqlConnStringOracle = @"";
        static private SqlConnection sqlConnOracle = new SqlConnection(sqlConnStringOracle);


        /*selectSgbd()
         * Devolve uma SQL Connection pronta a usar com um dos SGBDs existentes
         * Recebe:
         * - string sgbd para operar no switch e selecionar a SqlConnection
         * Devolve: SQL Connection por sgbd selecionado
        */

        static public Object selectSgbd(string sgbd)
        {
            Object sqlConn = null;

            try
            {
                switch (sgbd)
                {
                    case "BDlocal":
                        sqlConn = sqlConnBDLocal;
                        break;
                    case "SQLServer":
                        sqlConn = sqlConnSQLServer;
                        break;
                    case "MySql":
                        sqlConn = sqlConnMySql;
                        break;
                    case "SQLLite":
                        sqlConn = sqlConnSQLLite;
                        break;
                    case "Oracle":
                        sqlConn = sqlConnBDLocal;
                        break;
                    default:
                        MessageBox.Show("String Errada: " + sgbd, "ERRO: Utils - selectSGBD - sWITCH");
                        break;
                }
                return sqlConn;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Erro a abrir a base de dados \n" + ex.Message, "ERRO: UtilsSQL");
                return sqlConn;
            }
        }

        static public bool getTeste()
        {
            return teste;
        }

        static public void setTeste(bool t)
        {
           teste = t;
        }

        static public bool getBdOnline()
        {
            return bdOnline;
        }

        static public void setBdOnline(bool bd)
        {
            bdOnline = bd;
        }

        static public SqlConnection getSqlConnBDLocal()
        {
            return sqlConnBDLocal;
        }

        static public SqlConnection getSqlConnSQLServer()
        {
            return sqlConnSQLServer;
        }

        static public MySqlConnection getSqlConnMySql()
        {
            if (bdOnline) return sqlConnMySql = new MySqlConnection(sqlConnStringMySqlOnline);
            else return sqlConnMySql = new MySqlConnection(sqlConnStringMySqlLocal);

        }

        static public SQLiteConnection getSqlConnSQLLite()
        {
            return sqlConnSQLLite;
        }
        static public SqlConnection getSqlConnOracle()
        {
            return sqlConnOracle;
        }


    }
  
}
