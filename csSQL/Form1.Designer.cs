﻿namespace csSQL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuFicheiro = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDataSet = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDataSetAluno = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDataSetTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDlocal = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDlocalAlunoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDlocalAlunoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBDlocalTurmaInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDlocalTurmaConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBDlocalCicloLetivoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBDlocalCicloLeticoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSqlServer = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServerAlunoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServerAlunoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLServerTurmaInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServerTurmaConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLServerCicloLetivoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLServerCIcloLetivoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySql = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySqlAlunoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySqlAlunoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMySqlTurmaInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySqlConsultarTurma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuMySqlCicloLetivoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMySqlCicloLetivoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSqlLite = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLLiteAlunoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLLiteAlunoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLLiteTurmaInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLLiteTurmaConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.menuSQLLiteCicloLetivoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSQLLiteCicloLetivoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracle = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleAlunoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleAlunoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOracleTurmaInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleTurmaConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOracleCicloLetivoInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOracleCicloLetivoConsultar = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.radioButtonOff = new System.Windows.Forms.RadioButton();
            this.radioButtonOn = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonLocal = new System.Windows.Forms.RadioButton();
            this.radioButtonOnline = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.groupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFicheiro,
            this.menuDataSet,
            this.menuBDlocal,
            this.menuSqlServer,
            this.menuMySql,
            this.menuSqlLite,
            this.menuOracle});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(624, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuFicheiro
            // 
            this.menuFicheiro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sairToolStripMenuItem});
            this.menuFicheiro.Name = "menuFicheiro";
            this.menuFicheiro.Size = new System.Drawing.Size(61, 20);
            this.menuFicheiro.Text = "Ficheiro";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // menuDataSet
            // 
            this.menuDataSet.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuDataSetAluno,
            this.menuDataSetTurma});
            this.menuDataSet.Name = "menuDataSet";
            this.menuDataSet.Size = new System.Drawing.Size(59, 20);
            this.menuDataSet.Text = "DataSet";
            // 
            // menuDataSetAluno
            // 
            this.menuDataSetAluno.Name = "menuDataSetAluno";
            this.menuDataSetAluno.Size = new System.Drawing.Size(114, 22);
            this.menuDataSetAluno.Text = "Alunos";
            this.menuDataSetAluno.Click += new System.EventHandler(this.menuDataSetAluno_Click);
            // 
            // menuDataSetTurma
            // 
            this.menuDataSetTurma.Name = "menuDataSetTurma";
            this.menuDataSetTurma.Size = new System.Drawing.Size(114, 22);
            this.menuDataSetTurma.Text = "Turmas";
            this.menuDataSetTurma.Click += new System.EventHandler(this.menuDataSetTurma_Click);
            // 
            // menuBDlocal
            // 
            this.menuBDlocal.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBDlocalAlunoInserir,
            this.menuBDlocalAlunoConsultar,
            this.toolStripSeparator2,
            this.menuBDlocalTurmaInserir,
            this.menuBDlocalTurmaConsultar,
            this.toolStripSeparator1,
            this.menuBDlocalCicloLetivoInserir,
            this.menuBDlocalCicloLeticoConsultar});
            this.menuBDlocal.Name = "menuBDlocal";
            this.menuBDlocal.Size = new System.Drawing.Size(59, 20);
            this.menuBDlocal.Text = "BDlocal";
            // 
            // menuBDlocalAlunoInserir
            // 
            this.menuBDlocalAlunoInserir.Name = "menuBDlocalAlunoInserir";
            this.menuBDlocalAlunoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuBDlocalAlunoInserir.Text = "Inserir Aluno";
            this.menuBDlocalAlunoInserir.Click += new System.EventHandler(this.menuBDlocalAlunoInserir_Click);
            // 
            // menuBDlocalAlunoConsultar
            // 
            this.menuBDlocalAlunoConsultar.Name = "menuBDlocalAlunoConsultar";
            this.menuBDlocalAlunoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuBDlocalAlunoConsultar.Text = "Consultar Aluno";
            this.menuBDlocalAlunoConsultar.Click += new System.EventHandler(this.menuBDlocalAlunoConsultar_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(187, 6);
            // 
            // menuBDlocalTurmaInserir
            // 
            this.menuBDlocalTurmaInserir.Name = "menuBDlocalTurmaInserir";
            this.menuBDlocalTurmaInserir.Size = new System.Drawing.Size(190, 22);
            this.menuBDlocalTurmaInserir.Text = "Inserir Turma";
            this.menuBDlocalTurmaInserir.Click += new System.EventHandler(this.menuBDlocalTurmaInserir_Click);
            // 
            // menuBDlocalTurmaConsultar
            // 
            this.menuBDlocalTurmaConsultar.Name = "menuBDlocalTurmaConsultar";
            this.menuBDlocalTurmaConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuBDlocalTurmaConsultar.Text = "Consultar Turma";
            this.menuBDlocalTurmaConsultar.Click += new System.EventHandler(this.menuBDlocalTurmaConsultar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(187, 6);
            // 
            // menuBDlocalCicloLetivoInserir
            // 
            this.menuBDlocalCicloLetivoInserir.Name = "menuBDlocalCicloLetivoInserir";
            this.menuBDlocalCicloLetivoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuBDlocalCicloLetivoInserir.Text = "Inserir Ciclo Letivo";
            this.menuBDlocalCicloLetivoInserir.Click += new System.EventHandler(this.menuBDlocalCicloLetivoInserir_Click);
            // 
            // menuBDlocalCicloLeticoConsultar
            // 
            this.menuBDlocalCicloLeticoConsultar.Name = "menuBDlocalCicloLeticoConsultar";
            this.menuBDlocalCicloLeticoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuBDlocalCicloLeticoConsultar.Text = "Consultar Ciclo Letivo";
            this.menuBDlocalCicloLeticoConsultar.Click += new System.EventHandler(this.menuBDlocalCicloLeticoConsultar_Click);
            // 
            // menuSqlServer
            // 
            this.menuSqlServer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSQLServerAlunoInserir,
            this.menuSQLServerAlunoConsultar,
            this.toolStripSeparator3,
            this.menuSQLServerTurmaInserir,
            this.menuSQLServerTurmaConsultar,
            this.toolStripSeparator4,
            this.menuSQLServerCicloLetivoInserir,
            this.menuSQLServerCIcloLetivoConsultar});
            this.menuSqlServer.Name = "menuSqlServer";
            this.menuSqlServer.Size = new System.Drawing.Size(75, 20);
            this.menuSqlServer.Text = "SQL Server";
            // 
            // menuSQLServerAlunoInserir
            // 
            this.menuSQLServerAlunoInserir.Name = "menuSQLServerAlunoInserir";
            this.menuSQLServerAlunoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuSQLServerAlunoInserir.Text = "Inserir Aluno";
            this.menuSQLServerAlunoInserir.Click += new System.EventHandler(this.menuSQLServerAlunoInserir_Click);
            // 
            // menuSQLServerAlunoConsultar
            // 
            this.menuSQLServerAlunoConsultar.Name = "menuSQLServerAlunoConsultar";
            this.menuSQLServerAlunoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuSQLServerAlunoConsultar.Text = "Consultar Aluno";
            this.menuSQLServerAlunoConsultar.Click += new System.EventHandler(this.menuSQLServerAlunoConsultar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(187, 6);
            // 
            // menuSQLServerTurmaInserir
            // 
            this.menuSQLServerTurmaInserir.Name = "menuSQLServerTurmaInserir";
            this.menuSQLServerTurmaInserir.Size = new System.Drawing.Size(190, 22);
            this.menuSQLServerTurmaInserir.Text = "Inserir Turma";
            this.menuSQLServerTurmaInserir.Click += new System.EventHandler(this.menuSQLServerTurmaInserir_Click);
            // 
            // menuSQLServerTurmaConsultar
            // 
            this.menuSQLServerTurmaConsultar.Name = "menuSQLServerTurmaConsultar";
            this.menuSQLServerTurmaConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuSQLServerTurmaConsultar.Text = "Consultar Turma";
            this.menuSQLServerTurmaConsultar.Click += new System.EventHandler(this.menuSQLServerTurmaConsultar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(187, 6);
            // 
            // menuSQLServerCicloLetivoInserir
            // 
            this.menuSQLServerCicloLetivoInserir.Name = "menuSQLServerCicloLetivoInserir";
            this.menuSQLServerCicloLetivoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuSQLServerCicloLetivoInserir.Text = "Inserir Ciclo Letivo";
            this.menuSQLServerCicloLetivoInserir.Click += new System.EventHandler(this.menuSQLServerCicloLetivoInserir_Click);
            // 
            // menuSQLServerCIcloLetivoConsultar
            // 
            this.menuSQLServerCIcloLetivoConsultar.Name = "menuSQLServerCIcloLetivoConsultar";
            this.menuSQLServerCIcloLetivoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuSQLServerCIcloLetivoConsultar.Text = "Consultar Ciclo Letivo";
            this.menuSQLServerCIcloLetivoConsultar.Click += new System.EventHandler(this.menuSQLServerCIcloLetivoConsultar_Click);
            // 
            // menuMySql
            // 
            this.menuMySql.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuMySqlAlunoInserir,
            this.menuMySqlAlunoConsultar,
            this.toolStripSeparator5,
            this.menuMySqlTurmaInserir,
            this.menuMySqlConsultarTurma,
            this.toolStripSeparator6,
            this.menuMySqlCicloLetivoInserir,
            this.menuMySqlCicloLetivoConsultar});
            this.menuMySql.Name = "menuMySql";
            this.menuMySql.Size = new System.Drawing.Size(52, 20);
            this.menuMySql.Text = "MySql";
            // 
            // menuMySqlAlunoInserir
            // 
            this.menuMySqlAlunoInserir.Name = "menuMySqlAlunoInserir";
            this.menuMySqlAlunoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuMySqlAlunoInserir.Text = "Inserir Aluno";
            this.menuMySqlAlunoInserir.Click += new System.EventHandler(this.menuMySqlAlunoInserir_Click);
            // 
            // menuMySqlAlunoConsultar
            // 
            this.menuMySqlAlunoConsultar.Name = "menuMySqlAlunoConsultar";
            this.menuMySqlAlunoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuMySqlAlunoConsultar.Text = "Consultar Aluno";
            this.menuMySqlAlunoConsultar.Click += new System.EventHandler(this.menuMySqlAlunoConsultar_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(187, 6);
            // 
            // menuMySqlTurmaInserir
            // 
            this.menuMySqlTurmaInserir.Name = "menuMySqlTurmaInserir";
            this.menuMySqlTurmaInserir.Size = new System.Drawing.Size(190, 22);
            this.menuMySqlTurmaInserir.Text = "Inserir Turma";
            this.menuMySqlTurmaInserir.Click += new System.EventHandler(this.menuMySqlTurmaInserir_Click);
            // 
            // menuMySqlConsultarTurma
            // 
            this.menuMySqlConsultarTurma.Name = "menuMySqlConsultarTurma";
            this.menuMySqlConsultarTurma.Size = new System.Drawing.Size(190, 22);
            this.menuMySqlConsultarTurma.Text = "Consultar Turma";
            this.menuMySqlConsultarTurma.Click += new System.EventHandler(this.menuMySqlConsultarTurma_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(187, 6);
            // 
            // menuMySqlCicloLetivoInserir
            // 
            this.menuMySqlCicloLetivoInserir.Name = "menuMySqlCicloLetivoInserir";
            this.menuMySqlCicloLetivoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuMySqlCicloLetivoInserir.Text = "Inserir Ciclo Letivo";
            this.menuMySqlCicloLetivoInserir.Click += new System.EventHandler(this.menuMySqlCicloLetivoInserir_Click);
            // 
            // menuMySqlCicloLetivoConsultar
            // 
            this.menuMySqlCicloLetivoConsultar.Name = "menuMySqlCicloLetivoConsultar";
            this.menuMySqlCicloLetivoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuMySqlCicloLetivoConsultar.Text = "Consultar Ciclo Letivo";
            this.menuMySqlCicloLetivoConsultar.Click += new System.EventHandler(this.menuMySqlCicloLetivoConsultar_Click);
            // 
            // menuSqlLite
            // 
            this.menuSqlLite.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSQLLiteAlunoInserir,
            this.menuSQLLiteAlunoConsultar,
            this.toolStripSeparator7,
            this.menuSQLLiteTurmaInserir,
            this.menuSQLLiteTurmaConsultar,
            this.toolStripSeparator8,
            this.menuSQLLiteCicloLetivoInserir,
            this.menuSQLLiteCicloLetivoConsultar});
            this.menuSqlLite.Name = "menuSqlLite";
            this.menuSqlLite.Size = new System.Drawing.Size(62, 20);
            this.menuSqlLite.Text = "SQL Lite";
            // 
            // menuSQLLiteAlunoInserir
            // 
            this.menuSQLLiteAlunoInserir.Name = "menuSQLLiteAlunoInserir";
            this.menuSQLLiteAlunoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuSQLLiteAlunoInserir.Text = "Inserir Aluno";
            this.menuSQLLiteAlunoInserir.Click += new System.EventHandler(this.menuSQLLiteAlunoInserir_Click);
            // 
            // menuSQLLiteAlunoConsultar
            // 
            this.menuSQLLiteAlunoConsultar.Name = "menuSQLLiteAlunoConsultar";
            this.menuSQLLiteAlunoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuSQLLiteAlunoConsultar.Text = "Consultar Aluno";
            this.menuSQLLiteAlunoConsultar.Click += new System.EventHandler(this.menuSQLLiteAlunoConsultar_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(187, 6);
            // 
            // menuSQLLiteTurmaInserir
            // 
            this.menuSQLLiteTurmaInserir.Name = "menuSQLLiteTurmaInserir";
            this.menuSQLLiteTurmaInserir.Size = new System.Drawing.Size(190, 22);
            this.menuSQLLiteTurmaInserir.Text = "Inserir Turma";
            this.menuSQLLiteTurmaInserir.Click += new System.EventHandler(this.menuSQLLiteTurmaInserir_Click);
            // 
            // menuSQLLiteTurmaConsultar
            // 
            this.menuSQLLiteTurmaConsultar.Name = "menuSQLLiteTurmaConsultar";
            this.menuSQLLiteTurmaConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuSQLLiteTurmaConsultar.Text = "Consultar Turma";
            this.menuSQLLiteTurmaConsultar.Click += new System.EventHandler(this.menuSQLLiteTurmaConsultar_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(187, 6);
            // 
            // menuSQLLiteCicloLetivoInserir
            // 
            this.menuSQLLiteCicloLetivoInserir.Name = "menuSQLLiteCicloLetivoInserir";
            this.menuSQLLiteCicloLetivoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuSQLLiteCicloLetivoInserir.Text = "Inserir Ciclo Letivo";
            this.menuSQLLiteCicloLetivoInserir.Click += new System.EventHandler(this.menuSQLLiteCicloLetivoInserir_Click);
            // 
            // menuSQLLiteCicloLetivoConsultar
            // 
            this.menuSQLLiteCicloLetivoConsultar.Name = "menuSQLLiteCicloLetivoConsultar";
            this.menuSQLLiteCicloLetivoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuSQLLiteCicloLetivoConsultar.Text = "Consultar Ciclo Letivo";
            this.menuSQLLiteCicloLetivoConsultar.Click += new System.EventHandler(this.menuSQLLiteCicloLetivoConsultar_Click);
            // 
            // menuOracle
            // 
            this.menuOracle.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOracleAlunoInserir,
            this.menuOracleAlunoConsultar,
            this.toolStripSeparator9,
            this.menuOracleTurmaInserir,
            this.menuOracleTurmaConsultar,
            this.toolStripSeparator10,
            this.menuOracleCicloLetivoInserir,
            this.menuOracleCicloLetivoConsultar});
            this.menuOracle.Name = "menuOracle";
            this.menuOracle.Size = new System.Drawing.Size(53, 20);
            this.menuOracle.Text = "Oracle";
            // 
            // menuOracleAlunoInserir
            // 
            this.menuOracleAlunoInserir.Name = "menuOracleAlunoInserir";
            this.menuOracleAlunoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuOracleAlunoInserir.Text = "Inserir Aluno";
            this.menuOracleAlunoInserir.Click += new System.EventHandler(this.menuOracleAlunoInserir_Click);
            // 
            // menuOracleAlunoConsultar
            // 
            this.menuOracleAlunoConsultar.Name = "menuOracleAlunoConsultar";
            this.menuOracleAlunoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuOracleAlunoConsultar.Text = "Consultar Aluno";
            this.menuOracleAlunoConsultar.Click += new System.EventHandler(this.menuOracleAlunoConsultar_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(187, 6);
            // 
            // menuOracleTurmaInserir
            // 
            this.menuOracleTurmaInserir.Name = "menuOracleTurmaInserir";
            this.menuOracleTurmaInserir.Size = new System.Drawing.Size(190, 22);
            this.menuOracleTurmaInserir.Text = "Inserir Turma";
            this.menuOracleTurmaInserir.Click += new System.EventHandler(this.menuOracleTurmaInserir_Click);
            // 
            // menuOracleTurmaConsultar
            // 
            this.menuOracleTurmaConsultar.Name = "menuOracleTurmaConsultar";
            this.menuOracleTurmaConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuOracleTurmaConsultar.Text = "Consultar Turma";
            this.menuOracleTurmaConsultar.Click += new System.EventHandler(this.menuOracleTurmaConsultar_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(187, 6);
            // 
            // menuOracleCicloLetivoInserir
            // 
            this.menuOracleCicloLetivoInserir.Name = "menuOracleCicloLetivoInserir";
            this.menuOracleCicloLetivoInserir.Size = new System.Drawing.Size(190, 22);
            this.menuOracleCicloLetivoInserir.Text = "Inserir Ciclo Letivo";
            this.menuOracleCicloLetivoInserir.Click += new System.EventHandler(this.menuOracleCicloLetivoInserir_Click);
            // 
            // menuOracleCicloLetivoConsultar
            // 
            this.menuOracleCicloLetivoConsultar.Name = "menuOracleCicloLetivoConsultar";
            this.menuOracleCicloLetivoConsultar.Size = new System.Drawing.Size(190, 22);
            this.menuOracleCicloLetivoConsultar.Text = "Consultar Ciclo Letivo";
            this.menuOracleCicloLetivoConsultar.Click += new System.EventHandler(this.menuOracleCicloLetivoConsultar_Click);
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.radioButtonOff);
            this.groupBox.Controls.Add(this.radioButtonOn);
            this.groupBox.Location = new System.Drawing.Point(479, 27);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(133, 100);
            this.groupBox.TabIndex = 1;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Teste";
            // 
            // radioButtonOff
            // 
            this.radioButtonOff.AutoSize = true;
            this.radioButtonOff.Location = new System.Drawing.Point(21, 68);
            this.radioButtonOff.Name = "radioButtonOff";
            this.radioButtonOff.Size = new System.Drawing.Size(39, 17);
            this.radioButtonOff.TabIndex = 1;
            this.radioButtonOff.TabStop = true;
            this.radioButtonOff.Text = "Off";
            this.radioButtonOff.UseVisualStyleBackColor = true;
            // 
            // radioButtonOn
            // 
            this.radioButtonOn.AutoSize = true;
            this.radioButtonOn.Location = new System.Drawing.Point(21, 30);
            this.radioButtonOn.Name = "radioButtonOn";
            this.radioButtonOn.Size = new System.Drawing.Size(39, 17);
            this.radioButtonOn.TabIndex = 0;
            this.radioButtonOn.TabStop = true;
            this.radioButtonOn.Text = "On";
            this.radioButtonOn.UseVisualStyleBackColor = true;
            this.radioButtonOn.CheckedChanged += new System.EventHandler(this.radioButtonOn_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonLocal);
            this.groupBox1.Controls.Add(this.radioButtonOnline);
            this.groupBox1.Location = new System.Drawing.Point(479, 150);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(133, 100);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base De Dados";
            // 
            // radioButtonLocal
            // 
            this.radioButtonLocal.AutoSize = true;
            this.radioButtonLocal.Location = new System.Drawing.Point(21, 67);
            this.radioButtonLocal.Name = "radioButtonLocal";
            this.radioButtonLocal.Size = new System.Drawing.Size(51, 17);
            this.radioButtonLocal.TabIndex = 1;
            this.radioButtonLocal.TabStop = true;
            this.radioButtonLocal.Text = "Local";
            this.radioButtonLocal.UseVisualStyleBackColor = true;
            this.radioButtonLocal.CheckedChanged += new System.EventHandler(this.radioButtonLocal_CheckedChanged);
            // 
            // radioButtonOnline
            // 
            this.radioButtonOnline.AutoSize = true;
            this.radioButtonOnline.Location = new System.Drawing.Point(21, 34);
            this.radioButtonOnline.Name = "radioButtonOnline";
            this.radioButtonOnline.Size = new System.Drawing.Size(55, 17);
            this.radioButtonOnline.TabIndex = 0;
            this.radioButtonOnline.TabStop = true;
            this.radioButtonOnline.Text = "Online";
            this.radioButtonOnline.UseVisualStyleBackColor = true;
            this.radioButtonOnline.CheckedChanged += new System.EventHandler(this.radioButtonOnline_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 262);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "  ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuFicheiro;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuDataSet;
        private System.Windows.Forms.ToolStripMenuItem menuDataSetAluno;
        private System.Windows.Forms.ToolStripMenuItem menuDataSetTurma;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocal;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocalAlunoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocalAlunoConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocalTurmaInserir;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocalTurmaConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocalCicloLetivoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuBDlocalCicloLeticoConsultar;
        private System.Windows.Forms.ToolStripMenuItem menuSqlServer;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServerAlunoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServerAlunoConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServerTurmaInserir;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServerTurmaConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServerCicloLetivoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuSQLServerCIcloLetivoConsultar;
        private System.Windows.Forms.ToolStripMenuItem menuMySql;
        private System.Windows.Forms.ToolStripMenuItem menuMySqlAlunoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuMySqlAlunoConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem menuMySqlTurmaInserir;
        private System.Windows.Forms.ToolStripMenuItem menuMySqlConsultarTurma;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem menuMySqlCicloLetivoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuMySqlCicloLetivoConsultar;
        private System.Windows.Forms.ToolStripMenuItem menuSqlLite;
        private System.Windows.Forms.ToolStripMenuItem menuSQLLiteAlunoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuSQLLiteAlunoConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem menuSQLLiteTurmaInserir;
        private System.Windows.Forms.ToolStripMenuItem menuSQLLiteTurmaConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem menuSQLLiteCicloLetivoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuSQLLiteCicloLetivoConsultar;
        private System.Windows.Forms.ToolStripMenuItem menuOracle;
        private System.Windows.Forms.ToolStripMenuItem menuOracleAlunoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuOracleAlunoConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem menuOracleTurmaInserir;
        private System.Windows.Forms.ToolStripMenuItem menuOracleTurmaConsultar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem menuOracleCicloLetivoInserir;
        private System.Windows.Forms.ToolStripMenuItem menuOracleCicloLetivoConsultar;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.RadioButton radioButtonOff;
        private System.Windows.Forms.RadioButton radioButtonOn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonLocal;
        private System.Windows.Forms.RadioButton radioButtonOnline;
    }
}

